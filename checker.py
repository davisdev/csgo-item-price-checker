from dotenv import load_dotenv
import requests
import os
from urllib.parse import quote
from time import sleep
from bs4 import BeautifulSoup
import locale
import sqlite3
from threading import Thread

def create_price_table():
    db_file = os.getenv("DATABASE_FILE")

    connection = sqlite3.connect(db_file)
    cursor = connection.cursor()

    cursor.execute(
        '''CREATE TABLE IF NOT EXISTS prices (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            item_url TEXT NOT NULL,
            item_price DECIMAL(10) NOT NULL,
            created_at DATETIME DEFAULT CURRENT_TIMESTAMP
        );'''
    )

    cursor.close()


def setup():
    # Load all the environment variables
    load_dotenv()

    # Set currency format to native one
    locale.setlocale(locale.LC_ALL, '')

    create_price_table()

def get_item_price(response, format=False):
    price_decimal = response.get('sell_order_graph')[0][0]

    return price_decimal if format is False else locale.currency(price_decimal)

def persist_prices(price, url):
    from datetime import datetime

    db_file = os.getenv("DATABASE_FILE")

    connection = sqlite3.connect(db_file)
    cursor = connection.cursor()

    cursor.execute(
        "INSERT INTO prices (item_url, item_price, created_at) VALUES (?, ?, ?)", 
        (url, price, datetime.now())
    )

    connection.commit()

    cursor.close()

def get_price(url):
    while True:
        response = requests.get(url)

        price = get_item_price(response.json(), True)
        persist_prices(price, url)

        sleep(int(os.getenv("POLL_INTERVAL")))
        print("Performing a price check...")


def main():
    setup()  
          
    threads = []
    content = open(os.getenv('URLS_FILE'), 'r')

    # Creating a thread for each url
    for url in content.readlines():
        url = url.replace('\n', '')
        threads.append(Thread(target=get_price, args=(url,)))

    for thread in threads:
        thread.start()

if __name__ == "__main__":
    main()
